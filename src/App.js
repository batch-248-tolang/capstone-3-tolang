import { useState, useEffect } from 'react';
import { Col} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import { UserProvider } from './UserContext'

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Profile from './pages/Profile';
import AppNavbar from './components/AppNavbar';
import Dashboard from './components/Dashboard';
import ProductView from './components/ProductView';
import './App.css';

function App() {

  const [user, setUser] = useState({
    id:null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    fetch(`https://capstone-2-tolang.onrender.com/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{

      console.log(data);

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar/>
    <Col className="bg-color">
    <div className="home-bg">
    <Routes>
    <Route path="/" element={<Home/>} />
    <Route path="/products" element={<Products/>} />
    <Route path="/products/:productId" element={<ProductView/>} />
    <Route path="/login" element={<Login/>} />
    <Route path="/profile" element={<Profile/>} />
    <Route path="/logout" element={<Logout/>} />
    <Route path="/register" element={<Register/>} />
    <Route path="/admin" element={<Dashboard/>}/>          
    <Route path="*" element={<Error/>} />
    </Routes>
    </div>
    </Col>
    </Router>
    </UserProvider>

    );
  }

  export default App;

