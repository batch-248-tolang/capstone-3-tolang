import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import {Container, Row, Col} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products({ hideTitle }){

	const [ products, setProducts ] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch('https://capstone-2-tolang.onrender.com/products/')
		.then(res=>res.json())
		.then(data=>{

			const productArr = (data.map(product=>{

				return(

					<ProductCard productProp={product} />
					)
			}))
			setProducts(productArr)
		})
	},[products])

	return(

		user.isAdmin?
		<Navigate to="/admin" />
		:
		<Container>
		<h1 className="pt-3">Products</h1>
		<Row>
		{products.map((product) => (
		<Col key={product.props.productProp._id} sm={12} md={6} lg={4}>{product}
		</Col>
		))}
		</Row>
		</Container>
	);
};