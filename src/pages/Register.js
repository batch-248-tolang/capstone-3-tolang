import { useState, useEffect, useContext } from 'react';
import { Container, Row, Form, Button, Card, FloatingLabel } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [passwordStrength, setPasswordStrength] = useState ({
		hasUppercase: false,
		hasLowercase: false,
		hasNumber: false,
		hasSpecialChar: false,
		hasMinLength: false,
		isMatched: false,
	});

	useEffect(() => {
		setPasswordStrength(prevState => ({
			...prevState,
			hasUppercase: /[A-Z]/.test(password1),
			hasLowercase: /[a-z]/.test(password1),
			hasNumber: /[0-9]/.test(password1),
			hasSpecialChar: /[^A-Za-z0-9]/.test(password1),
			hasMinLength: password1.length >= 8,
			isMatched: password1 === password2 && password1.length > 0,
		}));
	}, [password1, password2])
		

	const [isActive, setIsActive] = useState(false);

	function registerUser(e){

		e.preventDefault();

		fetch(`https://capstone-2-tolang.onrender.com/users/checkEmail`,{
			method: "POST",
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data === true){

				Swal.fire({
					title: "EMAIL ALREADY EXIST",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			}else{

				fetch(`https://capstone-2-tolang.onrender.com/users/register`,{
					method:"POST",
					headers:{
						'Content-Type':'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res=>res.json())
				.then(data=>{

					if(data===true){

						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "REGISTRATION SUCCESSFUL",
							icon: "success",
							text: "Welcome Sikadista"
						})

						navigate("/login")

					}else{

						Swal.fire({
							title: "SOMETHING WENT WRONG",
							icon: "error",
							text: "Please try again!"

						})



					}
				})
			}
		})
	}


	useEffect(()=>{
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2) && passwordStrength.hasUppercase && passwordStrength.hasLowercase && passwordStrength.hasNumber && passwordStrength.hasSpecialChar && passwordStrength.hasMinLength){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password1, password2, passwordStrength])


	return (

		(user.id !== null)?
		<Navigate to="/products"/>
		:

		<Container>
		<Row className="register-page">
		<Card className="register-card">

		<h1>Register</h1>

		<Form onSubmit={(e)=>registerUser(e)}>
		<FloatingLabel
			controlId="firstName"
			label="First Name"
			className="mb-3 mt-3"
		>
		<Form.Control
			type="text"
			placeholder="Enter First Name"
			value = {firstName}
			onChange = {e=>setFirstName(e.target.value)}
			required
		/>
		</FloatingLabel>

		<FloatingLabel
			controlId="lastName"
			label="Last Name"
			className="mb-3"
		>
		<Form.Control
			type="text"
			placeholder="Enter Last Name"
			value = {lastName}
			onChange = {e=>setLastName(e.target.value)}
			required
		/>
		</FloatingLabel>

		<FloatingLabel
			controlId="email"
			label="Email"
			className="mb-3"
		>
		<Form.Control
			type="email"
			placeholder="Enter Email"
			value = {email}
			onChange = {e=>setEmail(e.target.value)}
			required
		/>
		</FloatingLabel>

		<FloatingLabel
			controlId="mobileNo"
			label="Mobile Number"
			className="mb-3"
		>
		<Form.Control
			type="text"
			placeholder="Enter Mobile Number"
			value = {mobileNo}
			onChange = {e=>setMobileNo(e.target.value)}
			required
		/>
		</FloatingLabel>

		<FloatingLabel
			controlId="password1"
			label="Password"
			className="mb-3"
		>
		<Form.Control
			type="password"
			placeholder="Enter Password"
			value = {password1}
			onChange = {e=>setPassword1(e.target.value)}
			required
		/>
		</FloatingLabel>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.hasMinLength} readOnly />
			8 Characters
			</label>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.hasUppercase} readOnly />
			Uppercase
			</label>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.hasLowercase} readOnly />
			Lowercase
			</label>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.hasNumber} readOnly />
			Number
			</label>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.hasSpecialChar} readOnly />
			Special Character
			</label>
		<FloatingLabel
			controlId="password2"
			label="Confirm Password"
			className="mb-3 mt-3"
		>
		<Form.Control
			type="password"
			placeholder="Confirm Password"
			value = {password2}
			onChange = {e=>setPassword2(e.target.value)}
			required
		/>
		</FloatingLabel>
			<label className="me-2">
			<input className="me-2" type="checkbox" checked={passwordStrength.isMatched} readOnly />
			Match
			</label>
			<br/>

		{isActive ?

		<Button variant="primary" type="submit" id="submitBtn" className="mt-3">
		Register
		</Button>

		:

		<Button variant="primary" type="submit" id="submitBtn" className="mt-3" disabled>
		Register
		</Button>

	}

	</Form>
	</Card>
	</Row>
	</Container>
	)
}