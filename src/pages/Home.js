import { useState, useEffect } from 'react';
import Highlights from '../components/Highlights';
import { Container, Row, Col } from "react-bootstrap";
import ProductCard from '../components/ProductCard';

export default function Home(){

	const [products, setProducts ] = useState([]);

	useEffect(() => {
		fetch('https://capstone-2-tolang.onrender.com/products/')
		.then(res=>res.json())
		.then(data=> {
			const productArr = (data.map(product=>{

				return(

					<ProductCard productProp={product} />
					)
			}))
			setProducts(productArr)
		})
	},[]);

	return (
		<Container className="d-flex flex-column justify-content-center">
		<>
		<div className="justify-content-center mb-1">
		<Highlights />
		</div>
		<div className="d-flex justify-content-center">
		</div>
		</>
		<Row className="mt-5">
		{products.map((product) => (
		<Col key={product.props.productProp._id} sm={12} md={6} lg={4}>{product}
		</Col>
		))}
		</Row>
		</Container>
		)
	}