import { useState, useEffect, useContext } from 'react';
import { Container, Row, Form, Button, Card, FloatingLabel } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");

	const [isActive, setIsActive] = useState(false);


	function authenticate(e){

		e.preventDefault();

		fetch('https://capstone-2-tolang.onrender.com/users/login',{
			method:'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password1
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				localStorage.setItem('email', email);

				retrieveUserDetails(data.access)

				Swal.fire({
					title:"LOGIN SUCCESSFUL",
					icon:"success",
					text:"Let's get your Sikad Gears"
				})
			}else{
				Swal.fire({
					title:"AUTHENTICATION ERROR",
					icon:"error",
					text:"Check your credentials!"
				})
			}
		})
		const retrieveUserDetails = (token) =>{

			fetch('https://capstone-2-tolang.onrender.com/users/details',{
				headers:{
					Authorization: `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}
		setEmail("");
		setPassword1("");
	}
	useEffect(()=>{

		if(email !== "" && password1 !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password1])


	return (

		(user.id !== null) ?
		<Navigate to="/products"/>
		:

		<Container>
		<Row className="login-page">
		<Card className="login-card">
		<h1>Login</h1>

		<Form onSubmit={(e)=>authenticate(e)}>

		<FloatingLabel
			controlId="userEmail"
			label="Enter Your Email"
			className="mb-3"
		>
		<Form.Control
			type="email"
			placeholder="Your Email"
			value = {email}
			onChange = {e=>setEmail(e.target.value)}
			required
		/>
		</FloatingLabel>

		<FloatingLabel
			controlId="password1"
			label="Password"
			className="mb-3"
		>
		<Form.Control
			type="password"
			placeholder="Enter Password"
			value = {password1}
			onChange = {e=>setPassword1(e.target.value)}
			required
		/>
		</FloatingLabel>

		{isActive ?

		<Button variant="success" type="submit" id="submitBtn">
		Login
		</Button>

		:

		<Button variant="secondary" type="submit" id="submitBtn" disabled>
		Login
		</Button>

	}

	</Form>
	</Card>
	</Row>
	</Container>
	)
}