import { Link } from 'react-router-dom'
import {Row, Col, Card, Button} from 'react-bootstrap';



export default function ProductCard({productProp}) {

  const {name, description, price, image, _id} = productProp;

  return (
    <Row className="mt-3 mb-3">
    <Col> 
    <Card className="main-card p-3" style={{ width: "auto", height: "30rem"}}>
    {image
    ?
    <Card.Img
    className="d-block w-100"
    src={image.secure_url}
    alt={name}
    />
    :
    null
    }
    <Card.Body className="text-center d-flex flex-column justify-content-between">
    <Row>
    <Card.Title>{name}</Card.Title>
    <Card.Subtitle>Description</Card.Subtitle>
    <Card.Text>
    {description}
    </Card.Text>
    </Row>
    <Row className="ps-5 pe-5 fixed-buttom">
    <Card.Subtitle>Price</Card.Subtitle>
    <Card.Text>
    {"\u20B1"} {price}
    </Card.Text>
    <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
    </Row>
    </Card.Body>
    </Card>
    </Col>
    </Row>
  )
}