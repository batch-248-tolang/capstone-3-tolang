import { useState, useEffect } from 'react';
import {Card} from 'react-bootstrap';

export default function Orders() {
	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch('https://capstone-2-tolang.onrender.com/products/all')
		.then(res => res.json())
		.then(data => {
			setOrders(data);
		})
		.catch(error => console.error(error));
	}, []);
	console.log(orders)
	return (
		<Card className="main-card mt-3 p-3">
		<Card.Title>List of Product Orders</Card.Title>
		{orders.map(product => (
			<Card className="secondary-card mt-3 p-3" key={product.name}>
			<Card.Body>
			<Card.Subtitle>Product Name:</Card.Subtitle>
			<Card.Text>{product.name}</Card.Text>
			<Card.Subtitle>Price:</Card.Subtitle>
			<Card.Text>{"\u20B1"} {product.price}</Card.Text>
			<Card.Subtitle>Orders from this Product:</Card.Subtitle>
			{Object.keys(product.orders).map((orderId) => (
				<Card className="tertiary-card mt-3 p-3" key={orderId}>
				<Card.Subtitle>Order ID:</Card.Subtitle>
				<Card.Text>{product.orders[orderId].orderId}</Card.Text>
				<Card.Subtitle>User ID:</Card.Subtitle>
				<Card.Text>{product.orders[orderId].userId}</Card.Text>
				<Card.Subtitle>Status:</Card.Subtitle>
				<Card.Text>{product.orders[orderId].isCancelled ? "Cancelled" : "Processing"}</Card.Text>
				<Card.Subtitle>Quantity:</Card.Subtitle>
				<Card.Text>{product.orders[orderId].quantity}</Card.Text>
				<Card.Subtitle>Ordered On:</Card.Subtitle>
				<Card.Text>{product.orders[orderId].purchasedOn}</Card.Text>
				</Card>
				))}
			</Card.Body>	
			</Card>
			))}
		</Card>
		);
};
