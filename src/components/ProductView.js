import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col, Form} from 'react-bootstrap';
import {Navigate, useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [productImg, setProductImg] = useState("");
	const [quantity, setQuantity] = useState(1);


	const checkout = (productId, quantity) => {

		fetch('https://capstone-2-tolang.onrender.com/users/checkout',{
			method: "POST",
			headers:{
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {

				Swal.fire({
					title:"SUCCESSFULLY ORDERED",
					icon:"success",
					text:"Thank you for gearing up!"
				})

				navigate("/products")

			}else{

				Swal.fire({
					title:"ERROR OCCURED",
					icon:"error",
					text:"Please try again"
				})

			}
		})
	}

	useEffect(() => {

		console.log(productId)

		fetch(`https://capstone-2-tolang.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setProductImg(data.image)
		})
	},[productId])

	return (
		user.isAdmin
		?
		<Navigate to="/admin" />
		:
		<Container className="p-3 product-view">
		<Row>
		<Col lg={{span:6, offset:3}}> 
		<Card className="main-card p-3 mt-3">
		{productImg
		?
		<Card.Img
		className="d-block w-100"
		src={productImg.url}
		alt={name}
		/>
		:
		null
	}
	<Card.Body  className="text-center d-flex flex-column justify-content-between">
	<Card.Title>{name}</Card.Title>
	<Card.Subtitle>Description</Card.Subtitle>
	<Card.Text>
	{description}
	</Card.Text>
	<Row className="fixed-buttom">
	<Card.Subtitle>Price</Card.Subtitle>
	<Card.Text>
	{"\u20B1"} {price}
	</Card.Text>
	</Row>
	<Row className="justify-content-center align-items-center fixed-buttom">
	<Form.Group controlId="quantity" className="mb-3" style={{width: "6rem"}}>
	<Form.Label>Quantity</Form.Label>
	<Form.Select type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)}>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	</Form.Select>
	</Form.Group>
	</Row>
	{
		(user.id !== null) ?
		<Row className="justify-content-center align-items-center fixed-buttom">
		<Button style={{width: "15rem"}} variant="primary" onClick={()=>checkout(productId, quantity)}>Checkout</Button>
		</Row>
		:
		<Row className="justify-content-center align-items-center fixed-buttom">
		<Link style={{width: "15rem"}} className="btn btn-danger" to="/login">Login to Checkout</Link>
		</Row>
	}	          
	</Card.Body>
	</Card>
	</Col>
	</Row>
	</Container>
	)
}