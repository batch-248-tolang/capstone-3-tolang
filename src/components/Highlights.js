import {Carousel} from 'react-bootstrap';
import { useState} from 'react';



export default function Highlights(){

	const [index, setIndex] = useState(0);

	const handleSelect = (selectedIndex, e) => {
	  setIndex(selectedIndex);

	}

	return(
		<Carousel activeIndex={index} onSelect={handleSelect}>

		<Carousel.Item>
			 <img
			    className="d-block w-100"
			    src="https://res.cloudinary.com/chriscloud1207/image/upload/v1681278762/sikad-gears/banner-2_ichvmw.png"
			    alt="First slide"
			  />
		</Carousel.Item>
      	<Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://res.cloudinary.com/chriscloud1207/image/upload/v1681278762/sikad-gears/banner-1_olv7tt.png"
	          alt="Second slide"
	        />
      	</Carousel.Item>
      	<Carousel.Item>
	        <img
	          className="d-block w-100"
	          src="https://res.cloudinary.com/chriscloud1207/image/upload/v1681278762/sikad-gears/banner-3_g7nnaw.png"
	          alt="Third slide"
	        />
      </Carousel.Item>
    </Carousel>
	)
}