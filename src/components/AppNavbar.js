import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return (

		<Navbar className="navbar-color" variant="dark" expand="lg">
		<Container>
		<Navbar.Brand as={Link} to="/"><img src="https://res.cloudinary.com/chriscloud1207/image/upload/v1681093348/sikad-gears/logo_bepfro.png" alt="Sikad Gears" style={{ width: 'auto', height: '40px' }} /></Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		<Navbar.Collapse id="basic-navbar-nav">
		<Nav className="me-auto">
		<Nav.Link as={Link} to="/" className="font-weight-bold fs-5">Home</Nav.Link>
		<Nav.Link as={Link} to="/products" className="font-weight-bold fs-5">Products</Nav.Link>

		{(user.id !== null)?

		<>
		<Nav.Link as={Link} to="/profile" className="font-weight-bold fs-5">My Profile</Nav.Link>

		<Nav.Link as={Link} to="/logout" className="font-weight-bold fs-5">Logout</Nav.Link>
		</>
		:

		<>
		<Nav.Link as={Link} to="/login" className="font-weight-bold fs-5">Login</Nav.Link>
		<Nav.Link as={Link} to="/register" className="font-weight-bold fs-5">Register</Nav.Link>
		</>
	}	
	</Nav>
	</Navbar.Collapse>
	</Container>
	</Navbar>
	)
}